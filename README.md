# BDClimaAPP

[Instruções de instalação do react-native](https://facebook.github.io/react-native/docs/getting-started#running-your-react-native-application)

## Instalação do app para debug
```sh    
    # Dentro do diretório do projeto
    react-native run-android
```

## Execução
```sh
    npm install
    npm run start
```