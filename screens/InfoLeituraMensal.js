import React, { Fragment } from 'react'

import {
    View,
    Text, 
    StyleSheet,
    TouchableOpacity
} from 'react-native'

import {
    Link
} from 'react-router-native'

import Informacoes from './Informacoes'
import {leitura_mensal} from '../InfoTemplates'

import Form from './Form'

class InfoLeituraMensal extends React.Component {
    constructor(props) {
        super(props)
        this.state = { leitura: {}, isEditing: false }
    }

    componentDidMount() {
        this.getLeitura()
    }

    async getLeitura(){
        fetch(`http://192.168.1.6:3000/lm${this.props.location.search}`).then(res => {
            res.json().then(json => {
                this.setState({
                    leitura: json
                })
            })
        }).catch(err => {
            alert(err)
        })
    }

    async deleteRecord(){
        fetch(`http://192.168.1.6:3000/lm${this.props.location.search}`, {method: 'DELETE'}).catch( err => { alert(err) })
    }

    toggleEdit(){
        this.setState({isEditing: !this.state.isEditing})
    }

    submitRecord(record){
        fetch(`http://192.168.1.6:3000/lm${this.props.location.search}`, { 
            method: 'PUT', 
            body: JSON.stringify(record),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).catch(err => console.log(err))
        this.toggleEdit()
        this.getLeitura()
    }

    render() {
        return (
            <Fragment>
                {
                    this.state.isEditing 
                    ? <Form template={leitura_mensal} defaultValues={this.state.leitura} onCancel={this.toggleEdit.bind(this)} onSubmit={this.submitRecord.bind(this)}/>
                    : <Informacoes template={leitura_mensal} values={this.state.leitura} onDelete={this.deleteRecord.bind(this)} onEdit={this.toggleEdit.bind(this)}/>
                }
                { 
                    this.state.leitura["Estacao_OMM"]
                    ? (<Link style={styles.retornar} to={`/${this.state.leitura.Estacao_OMM}`}>
                            <Text>Retornar</Text>
                        </Link>)
                    : null
                }
            </Fragment>
        )
    }
}

const styles = StyleSheet.create({
    retornar: {
        borderColor: "#666",
        borderWidth: 1,
        borderRadius: 10,
        marginHorizontal: 20,
        marginTop: 30,
        marginBottom: 10,
        padding: 10
    },
})

export default InfoLeituraMensal