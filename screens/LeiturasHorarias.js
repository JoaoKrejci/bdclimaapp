import React from 'react'

import {
    View,
    Text,
    StyleSheet
} from 'react-native'

import {
    Link
} from 'react-router-native'

import queryString from 'query-string'
import moment from 'moment'

class LeiturasHorarias extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            leituras: []
        }

        this.query = queryString.parse(props.location.search)
    }

    async getLeituras() {
        fetch(`http://192.168.1.6:3000/lhs?omm=${this.props.match.params.omm}&dini=${this.query.dini}&dfim=${this.query.dfim}`).then(res => {
            res.json().then(json => {
                this.setState({ leituras: json })
            })
        }).catch(err => {
            //alert(err)
        })
    }

    componentDidMount() {
        this.getLeituras()
    }

    render() {
        return (
            <View style={styles.leituras}>
                <Link style={styles.leitura} to="/insert_lh"><Text>Inserir Nova Leitura</Text></Link>
                {this.state.leituras.map((leitura, key) => (
                    <Link style={styles.leitura} to={`/lh?omm=${leitura.OMM}&dia=${moment(leitura.Dia).format("YYYY-MM-DD")}&hora=${leitura.Hora}`} key={key}>
                        <View key={key}>                            
                            <Text>{moment(leitura.Dia).format("DD/MM/YYYY")}</Text>
                            <Text>{leitura.Hora}</Text>
                        </View>
                    </Link>
                ))}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    leituras: {
        marginTop: 20
    },
    leitura: {
        marginHorizontal: 20,
        marginVertical: 10,
        borderColor: "#666",
        borderWidth: 1,
        padding: 10,
        borderRadius: 10
    }
}) 

export default LeiturasHorarias