import React, { Fragment } from 'react'
import {
    TextInput,
    Text,
    View,
    ScrollView,
    StyleSheet,
    TouchableOpacity
} from 'react-native'

class Form extends React.Component {
    constructor(props) {
        super(props)
        this.wie = this._wie.bind(this)
        this.values = {}
        props.template.map(key => {
            this.values[key.key] = props.defaultValues[key.key] || null
        })
    }

    _wie(key) {
        return this.props.defaultValues[key] || ''
    }

    updateValue(value, key) {        
        this.values[key] = value
    }

    componentDidUpdate() {
        this.props.template.map(key => {
            this.values[key.key] = this.props.defaultValues[key.key] || null
        })
    }

    render() {
        return (
            <Fragment>
                <ScrollView style={styles.view} contentContainerStyle={styles.scrollContainer}>
                    {this.props.template.map(({ name, key }, index) => (
                        <View key={index}>
                            <Text>{name}</Text><TextInput ref={key} style={styles.input} onChangeText={ value => { this.updateValue(value, key) } } defaultValue={this.wie(key) + ''}></TextInput>
                        </View>
                    ))}
                </ScrollView>
                <TouchableOpacity style={styles.submitbutton} onPress={this.props.onSubmit.bind(this, this.values)} ><Text style={{ color: 'white' }}>SUBMETER</Text></TouchableOpacity>
                <TouchableOpacity style={styles.cancelbutton} onPress={this.props.onCancel} ><Text style={{ color: 'white' }}>CANCELAR</Text></TouchableOpacity>
            </Fragment>
        )
    }
}

const styles = StyleSheet.create({
    view: {
        margin: 20,
        borderWidth: 1,
        borderColor: "#666",
        padding: 10,
        borderRadius: 4,
        flex: 1,
    },
    scrollContainer: {
        flexGrow: 1,
        paddingBottom: 20
    },
    input: {
        borderWidth: 1,
        padding: 0,
        paddingLeft: 10,
        marginBottom: 4,
        borderRadius: 4,
    },
    submitbutton: {
        height: 30,
        borderRadius: 4,
        backgroundColor: "#6d6",
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 20,
    },
    cancelbutton: {
        height: 30,
        borderRadius: 4,
        backgroundColor: "#d66",
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 4,
        marginHorizontal: 20,
    }
})

export default Form