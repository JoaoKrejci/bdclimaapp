import React from 'react'
import {
  View,
  ScrollView,
  Text,
  StyleSheet
} from 'react-native'

import { Link } from 'react-router-native'

class EstacoesList extends React.Component {
  constructor(props) {
		super(props)

		this.state = {
			estacoes: []
		}
	}

	async getEstacoes() {
		fetch("http://192.168.1.6:3000/estacoes").then(res => {
			res.json().then(json => {
				this.setState({estacoes: json})
			})
		}).catch(err => { console.log(err) })
	}


	componentDidMount() {
		this.getEstacoes()
	}
  
  render() {    
    return (
      <ScrollView style={styles.estacoesList}>
        <Link style={styles.estacoesListEstacao} to="/insert_estacao"><Text>Inserir Nova Estação</Text></Link>
        {this.state.estacoes.map((estacao, key) => (
          <Link to={`/${estacao.OMM}`} key={key}>
            <View style={styles.estacoesListEstacao} >
              <Text>{estacao.OMM}</Text>
              <Text>{estacao.NomeEstacao}</Text>
            </View>
          </Link>
        ))}

      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  estacoesList: {
    paddingHorizontal: 10,    
  },
  estacoesListEstacao: {
    marginTop: 10,
    padding: 10,
    borderColor: "#666",
    borderRadius: 4,
    borderWidth: 1,
    
  },
  OMM: {
    fontSize: 20,
  },
  NomeEstacao: {
    fontSize: 14
  }
})

export default EstacoesList