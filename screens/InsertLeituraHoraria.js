import React, { Fragment } from 'react'

import {
    Redirect
} from 'react-router-native'

import Form from './Form'
import { leitura_horaria } from '../InfoTemplates'

class InsertLeituraHoraria extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            redirect: false
        }
    }

    redirect(){
        this.setState({redirect: true})
    }

    submit(record){
        fetch(`http://192.168.1.6:3000/lh`, { 
            method: 'POST', 
            body: JSON.stringify(record),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).catch(err => console.log(err))
    }

    render() {
        return (
            <Fragment>
                {
                    this.state.redirect
                        ? <Redirect to="/" />
                        : < Form template={leitura_horaria} defaultValues={{}} onSubmit={this.submit.bind(this)} onCancel={this.redirect.bind(this)} />
                }
            </Fragment>
        )
    }
}

export default InsertLeituraHoraria