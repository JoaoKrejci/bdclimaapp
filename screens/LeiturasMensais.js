import React from 'react'
import {
    Text,
    View,
    StyleSheet
} from 'react-native'

import {
    Link
} from 'react-router-native'

import queryString from 'query-string'


class LeiturasMensais extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            leituras: []
        }

        this.query = queryString.parse(props.location.search)        
    }

    async getLeituras() {
        fetch(`http://192.168.1.6:3000/lms?omm=${this.props.match.params.omm}&aini=${this.query.aini}&afim=${this.query.afim}&mini=${this.query.mini}&mfim=${this.query.mfim}`).then(res => {
            res.json().then(json => {
                this.setState({ leituras: json })
            })
        }).catch(err => {
            alert(err)
        })
    }

    componentDidMount() {
        this.getLeituras()
    }

    render() {
        return (
            <View style={styles.leituras}>
                <Link style={styles.leitura} to="/insert_lm"><Text>Inserir Nova Leitura</Text></Link>
                {this.state.leituras.map((leitura, key) => (
                    <Link style={styles.leitura} to={`/lm?omm=${leitura.Estacao_OMM}&ano=${leitura.Ano}&mes=${leitura.Mes}`} key={key}>
                        <View key={key}>                            
                            <Text>{leitura.Mes}/{leitura.Ano}</Text>
                        </View>
                    </Link>
                ))}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    leituras: {
        marginTop: 20
    },
    leitura: {
        marginHorizontal: 20,
        marginVertical: 10,
        borderColor: "#666",
        borderWidth: 1,
        padding: 10,
        borderRadius: 10
    }
}) 

export default LeiturasMensais