import React, {Fragment} from 'react'

import {
    StyleSheet,
    Text,    
    View,
    TouchableOpacity
} from 'react-native'

import Icon from 'react-native-vector-icons/MaterialIcons'

class Informacoes extends React.Component {
    constructor(props){
        super(props)        
        this.omm = props.template[0]
        this.wie = this._wie.bind(this)
    }

    _wie(key){
        return this.props.values[key] || null
    }    

    render(){
        return (
            <Fragment>
                <View style={styles.title}>
                    <Text>{this.wie(this.omm.key)}</Text>
                    <Text>{this.wie("NomeEstacao")}</Text>
                </View>                

                <View style={styles.informacoes}>
                    {
                        this.props.template.map( ({key,name}, index) => (
                            <Text key={index}>{name}: {this.wie(key)}</Text>
                        ))
                    }
                </View>

                <View style={styles.operacoes}>
                    <TouchableOpacity style={styles.funcbutton} onPress={ this.props.onEdit }>
                        <Icon 
                            name="create"
                            color="#3a4"
                            size={24}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.funcbutton} onPress={ this.props.onDelete }>
                        <Icon 
                            name="delete"
                            color="#f66"
                            size={24}
                        />
                    </TouchableOpacity>
                </View>
            </Fragment>
        )
    }
}

const styles = StyleSheet.create({
    title: {
        marginTop: 20,
        marginLeft: 20
    },
    informacoes: {
        margin: 20,
        padding: 10,
        borderWidth: 1,
        borderColor: "#666",
        borderRadius: 10
    },
    operacoes: {
        flexDirection: "row",
        justifyContent: "flex-end",
        marginRight: 20,
    },
    funcbutton: {
        marginLeft: 20,
        borderRadius: 50,
        borderWidth: 1,
        padding: 5,
        borderColor: "#666"
    }
})

export default Informacoes