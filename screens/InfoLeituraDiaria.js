import React, { Fragment } from 'react'

import {
    Text,
    StyleSheet,
} from 'react-native'

import {
    Link
} from 'react-router-native'

import Informacoes from './Informacoes'
import { leitura_diaria } from '../InfoTemplates'

import Form from './Form'

class InfoLeituraDiaria extends React.Component {
    constructor(props) {
        super(props)
        this.state = { leitura: {}, isEditing: false }
    }

    async getLeitura(){
        fetch(`http://192.168.1.6:3000/ld${this.props.location.search}`).then(res => {
            res.json().then(json => {
                console.log(json);
                this.setState({
                    leitura: json
                })
            })
        }).catch(err => {
            alert(err)
        })
    }

    componentDidMount() {        
        this.getLeitura()
    }

    async deleteRecord(){
        fetch(`http://192.168.1.6:3000/ld${this.props.location.search}`, {method: 'DELETE'}).catch( err => { alert(err) })
    }

    toggleEdit(){
        this.setState({isEditing: !this.state.isEditing})
    }

    submitRecord(record){        
        fetch(`http://192.168.1.6:3000/ld${this.props.location.search}`, { 
            method: 'PUT', 
            body: JSON.stringify(record),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).catch(err => console.log(err))
        this.toggleEdit()
        this.getLeitura()
    }

    render() {
        return (
            <Fragment>
                {
                    this.state.isEditing 
                    ? <Form template={leitura_diaria} defaultValues={this.state.leitura} onCancel={this.toggleEdit.bind(this)} onSubmit={this.submitRecord.bind(this)}/>
                    : <Informacoes template={leitura_diaria} values={this.state.leitura} onDelete={this.deleteRecord.bind(this)} onEdit={this.toggleEdit.bind(this)}/>
                }
                {
                    this.state.leitura["Estacao_OMM"]
                        ? (
                            <Link style={styles.retornar} to={`/${this.state.leitura.Estacao_OMM}`}>
                                <Text>Retornar</Text>
                            </Link>
                        )
                        : null
                }
            </Fragment>
        )
    }
}

const styles = StyleSheet.create({
    informacoes: {
        margin: 20,
        padding: 10,
        borderWidth: 1,
        borderColor: "#666",
        borderRadius: 10
    },
    retornar: {
        borderColor: "#666",
        borderWidth: 1,
        borderRadius: 10,
        marginHorizontal: 20,
        marginTop: 40,
        padding: 10
    },
    operacoes: {
        flexDirection: "row",
        justifyContent: "flex-end",
        marginRight: 20,
    },
    funcbutton: {
        marginLeft: 20,
        borderRadius: 50,
        borderWidth: 1,
        padding: 5,
        borderColor: "#666"
    }
})

export default InfoLeituraDiaria