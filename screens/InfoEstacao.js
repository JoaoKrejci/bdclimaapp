import React from 'react'

import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    DatePickerAndroid,
} from 'react-native'

import {
    Route
} from 'react-router'

import {
    Link
} from 'react-router-native'

import Icon from 'react-native-vector-icons/MaterialIcons'

import LeiturasHorarias from './LeiturasHorarias'
import LeiturasDiarias from './LeiturasDiarias'
import LeiturasMensais from './LeiturasMensais'

import Informacoes from './Informacoes'
import Form from './Form'
import { estacao } from '../InfoTemplates'

class InfoEstacao extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            estacao: {},
            dtInicio: {
                year: 2013,
                month: 4,
                day: 28
            },
            dtFim: {
                year: 2013,
                month: 4,
                day: 29
            },
            isEditing: false
        }
        this.wie = this._wie.bind(this)
    }

    componentDidMount() {
        fetch(`http://192.168.1.6:3000/estacao/${this.props.match.params.omm}`).then(res => {
            res.json().then(json => {
                this.setState({
                    estacao: json
                })
            })
        }).catch(err => {
            alert(err)
        })
    }

    _wie(field) { // Write If Exists
        return this.state.estacao[field] ? this.state.estacao[field] : null
    }

    async getDate(dt) {
        let { action, year, month, day } = await DatePickerAndroid.open({
            date: !dt
                ? new Date(
                    this.state.dtInicio.year,
                    this.state.dtInicio.month - 1,
                    this.state.dtInicio.day
                )
                : new Date(
                    this.state.dtFim.year,
                    this.state.dtFim.month - 1,
                    this.state.dtFim.day
                )
        })

        if (year !== undefined)
            if (!dt)
                this.setState({
                    dtInicio: {
                        year: year,
                        month: month + 1,
                        day: day
                    }
                })

            else
                this.setState({
                    dtFim: {
                        year: year,
                        month: month + 1,
                        day: day
                    }
                })
    }

    toDateString(date) {
        return `${date.year}-${date.month}-${date.day}`
    }

    async submitRecord(record) {        
        fetch(`http://192.168.1.6:3000/estacao/${this.props.match.params.omm}`, { 
            method: 'PUT', 
            body: JSON.stringify(record),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).catch(err => console.log(err))
        this.toggleEdit()

        fetch(`http://192.168.1.6:3000/estacao/${this.props.match.params.omm}`).then(res => {
            res.json().then(json => {
                this.setState({
                    estacao: json
                })
            })
        }).catch(err => {
            alert(err)
        })
    }

    async deleteRecord() {
        fetch(`http://192.168.1.6:3000/estacao/${this.props.match.params.omm}`, {method: 'DELETE'}).catch( err => { alert(err) })
    }

    toggleEdit() {
        this.setState({ isEditing: !this.state.isEditing })
    }

    render() {
        return (
            <ScrollView>
                {
                    this.state.isEditing
                        ? <Form template={estacao} defaultValues={this.state.estacao} onSubmit={this.submitRecord.bind(this)} onCancel={this.toggleEdit.bind(this)} />
                        : <Informacoes template={estacao} values={this.state.estacao} onDelete={this.deleteRecord.bind(this)} onEdit={this.toggleEdit.bind(this)} />
                }

                <Text style={styles.leituras}>Leituras</Text>

                <View style={styles.calendarios}>
                    <Text>Data Inicial</Text>
                    <TouchableOpacity style={styles.calendario} onPress={this.getDate.bind(this, 0)}>
                        <Icon
                            name='event'
                            size={20}
                            color='#555' />
                        <Text>{`${this.state.dtInicio.day}/${this.state.dtInicio.month}/${this.state.dtInicio.year}`}</Text>
                    </TouchableOpacity>
                    <Text>Data Final</Text>
                    <TouchableOpacity style={styles.calendario} onPress={this.getDate.bind(this, 1)}>
                        <Icon
                            name='event'
                            size={20}
                            color='#555' />
                        <Text>{`${this.state.dtFim.day}/${this.state.dtFim.month}/${this.state.dtFim.year}`}</Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.buttons}>
                    <Link
                        to={`/${this.props.match.params.omm}/lh?dini=${this.toDateString(this.state.dtInicio)}&dfim=${this.toDateString(this.state.dtFim)}`}
                        style={styles.button}>
                        <Text>Horárias</Text>
                    </Link>
                    <Link
                        to={`/${this.props.match.params.omm}/ld?dini=${this.toDateString(this.state.dtInicio)}&dfim=${this.toDateString(this.state.dtFim)}`}
                        style={styles.button}>
                        <Text>Diárias</Text>
                    </Link>
                    <Link
                        to={`/${this.props.match.params.omm}/lm?mini=${this.state.dtInicio.month}&aini=${this.state.dtInicio.year}&mfim=${this.state.dtFim.month}&afim=${this.state.dtFim.year}`}
                        style={styles.button}>
                        <Text>Mensais</Text>
                    </Link>
                </View>

                <Route path="/:omm/lh" component={LeiturasHorarias} />
                <Route path="/:omm/ld" component={LeiturasDiarias} />
                <Route path="/:omm/lm" component={LeiturasMensais} />
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        padding: 20
    },
    nomeestacao: {
        fontSize: 18,
        fontWeight: "bold"
    },
    info: {
        padding: 6,
        margin: 20,
        borderColor: "#666",
        borderRadius: 10,
        borderWidth: 1
    },
    calendarios: {
        marginHorizontal: 20,
    },
    calendario: {
        flexDirection: 'row-reverse',
        marginVertical: 10,
        justifyContent: "space-between",
        borderColor: "#666",
        borderBottomWidth: 1,
        paddingBottom: 4,
    },
    leituras: {
        fontWeight: "bold",
        marginLeft: 20
    },
    buttons: {
        flexDirection: "row",
        justifyContent: "space-around",
        marginTop: 10,
        paddingHorizontal: 10,
    },
    button: {
        borderWidth: 1,
        borderColor: "#344",
        borderRadius: 10,
        height: 30,
        width: 80,
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginHorizontal: 10
    },
})

export default InfoEstacao