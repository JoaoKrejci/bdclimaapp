const
    estacao = [
        { name: 'OMM', key: 'OMM' },
        { name: 'Nome da Estação', key: 'NomeEstacao' },
        { name: 'Latitude', key: 'Latitude' },
        { name: 'Longitude', key: 'Longitude' },
        { name: 'Altitude', key: 'Altitude' },
        { name: 'Operante', key: 'Operante' },
        { name: 'Inicio da Operação', key: 'InicioOperacao'},
        { name: 'Fim da Operação', key: 'FimOperacao' },
        { name: 'Cidade', key: 'Cidade' },
        { name: 'Estado', key: 'Estado' },
        { name: 'Região', key: 'Regiao' }
    ],
    leitura_horaria = [
        { name: 'OMM da Estacao', key: 'Estacao_OMM' },
        { name: 'Dia', key: 'Dia' },
        { name: 'Hora', key: 'Hora' },
        { name: 'Temperatura do Bulbo Seco', key: 'TemperaturaBulboSeco' },
        { name: 'Temperatura do Bulbo Umido', key: 'TemperaturaBulboUmido' },
        { name: 'Temperatura Máxima', key: 'TemperaturaMaxima' },
        { name: 'Temperatura Minima', key: 'TemperaturaMinima' },
        { name: 'Sensação Termica', key: 'SensacaoTermica' },
        { name: 'Umidade Relativa', key: 'UmidadeRelativa' },
        { name: 'Umidade Relativa Máxima', key: 'UmidadeRelativaMaxima' },
        { name: 'Umidade Relativa Mínima', key: 'UmidadeRelativaMinima' },
        { name: 'Velocidade Média do Vento', key: 'VelocidadeMediaVento' },
        { name: 'Direção Predominante do Vento', key: 'DirecaoPredominanteVento' },
        { name: 'Pressão Atmosférica', key: 'PressaoAtmosferica' }
    ],
    leitura_diaria = [
        { name: 'OMM da Estacao', key: 'Estacao_OMM' },
        { name: 'Dia', key: 'Dia' },
        { name: 'Precipitacao', key: 'Precipitacao' },
        { name: 'Temperatura Compensada Média', key: 'TemperaturaCompensadaMedia' },
        { name: 'Temperatura Máxima', key: 'TemperaturaMaxima' },
        { name: 'Temperatura Mínima', key: 'TemperaturaMinima' },
        { name: 'Insolação', key: 'Insolacao' },
        { name: 'Evaporacao do Piche', key: 'EvaporacaoPiche' },
        { name: 'Umidade Relativa Media', key: 'UmidadeRelativaMedia' },
        { name: 'Velocidade Media do Vento', key: 'VelocidadeMediaVento' }
    ],
    leitura_mensal = [
        { name: 'OMM da Estacao', key: 'Estacao_OMM' },
        { name: 'Ano', key: 'Ano' },
        { name: 'Mes', key: 'Mes' },
        { name: 'Direcao Predominante do Vento', key: 'DirecaoPredominanteVento' },
        { name: 'Velocidade Media Vento', key: 'VelocidadeMediaVento' },
        { name: 'Velocidade Maxima Media do Vento', key: 'VelocidadeMaximaMediaVento' },
        { name: 'Evaporacao do Piche', key: 'EvaporacaoPiche' },
        { name: 'Evapotranspiracao Potencial BH', key: 'EvapotranspiracaoPotencialBH' },
        { name: 'Evapotranspiracao Real BH', key: 'EvapotranspiracaoRealBH' },
        { name: 'Insolacao Total', key: 'InsolacaoTotal' },
        { name: 'Nebulosidade Media', key: 'NebulosidadeMedia' },
        { name: 'Dias Precipitacao', key: 'DiasPrecipitacao' },
        { name: 'Precipitacao Total', key: 'PrecipitacaoTotal' },
        { name: 'Pressão Atmosférica Mar Media', key: 'PressaoAtmosfericaMarMedia' },
        { name: 'Pressao Atmosférica Media', key: 'PressaoAtmosfericaMedia' },
        { name: 'Temperatura Máxima Média', key: 'TemperaturaMaximaMedia' },
        { name: 'Temperatura Compensada Média', key: 'TemperaturaCompensadaMedia' },
        { name: 'Temperatura Mínima Média', key: 'TemperaturaMinimaMedia' },
        { name: 'Visibilidade Média', key: 'VisibilidadeMedia' }
    ]

export { estacao }
export { leitura_horaria }
export { leitura_diaria }
export { leitura_mensal }