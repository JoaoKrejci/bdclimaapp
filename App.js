import React, { Fragment } from 'react';
import {
	StyleSheet,
	View,
	StatusBar,
	Text,	
} from 'react-native';
import {
	Route,
	Switch
} from 'react-router'
import {
	NativeRouter,
	BackButton,	
} from 'react-router-native'

import EstacoesList from './screens/EstacoesList'
import InfoEstacao from './screens/InfoEstacao'
import InfoLeituraHoraria from './screens/InfoLeituraHoraria'
import InfoLeituraDiaria from './screens/InfoLeituraDiaria'
import InfoLeituraMensal from './screens/InfoLeituraMensal'
import InsertEstacao from './screens/InsertEstacao'
import InsertLeituraHoraria from './screens/InsertLeituraHoraria'
import InsertLeituraDiaria from './screens/InsertLeituraDiaria';
import InsertLeituraMensal from './screens/InsertLeituraMensal';


class App extends React.Component {
	render() {
		return (
			<Fragment>
				<StatusBar backgroundColor="#344" barStyle="light-content" />
				<View style={styles.headerBar}>
					<Text style={styles.title}>BDClima App</Text>
				</View>
					<NativeRouter>
						<BackButton>
							<Switch>
								<Route exact path="/" component={EstacoesList} />
								<Route path="/lh" component={InfoLeituraHoraria} />
								<Route path="/ld" component={InfoLeituraDiaria} />
								<Route path="/lm" component={InfoLeituraMensal} />
								<Route path="/insert_lh" component={InsertLeituraHoraria} />
								<Route path="/insert_ld" component={InsertLeituraDiaria} />
								<Route path="/insert_lm" component={InsertLeituraMensal} />
								<Route path="/insert_estacao" component={InsertEstacao} />
								<Route path="/:omm" component={InfoEstacao} />
							</Switch>
						</BackButton>
					</NativeRouter>				
			</Fragment>
		)
	}
}

const styles = StyleSheet.create({
	headerBar: {
		backgroundColor: "#566",
		height: 50,
		padding: 10,
		elevation: 2
	},
	title: {
		color: "white",
		fontSize: 24,
	}
});

export default App;
